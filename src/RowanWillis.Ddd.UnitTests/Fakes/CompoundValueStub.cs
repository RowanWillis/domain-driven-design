﻿namespace RowanWillis.Ddd.UnitTests;

public class CompoundValueStub<TValue1, TValue2> : Value
{
	public CompoundValueStub(TValue1 value1, TValue2 value2)
	{
		Value1 = value1;
		Value2 = value2;
	}

	private readonly TValue1 Value1;

	private readonly TValue2 Value2;

	protected override IEnumerable<object?> EqualityComponents => new object?[] { Value1, Value2 };
}