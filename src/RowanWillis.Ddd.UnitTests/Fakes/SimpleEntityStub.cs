﻿namespace RowanWillis.Ddd.UnitTests;

public class SimpleEntityStub<TIdentity> : Entity<TIdentity>
{
	public SimpleEntityStub(TIdentity id) : base(id) {}
}