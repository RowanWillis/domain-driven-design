﻿namespace RowanWillis.Ddd.UnitTests;

public class SimpleValueStub<TValue> : Value
{
	public SimpleValueStub(TValue value)
	{
		Value = value;
	}

	private readonly TValue Value;

	protected override IEnumerable<object?> EqualityComponents => new object?[] { Value };
}