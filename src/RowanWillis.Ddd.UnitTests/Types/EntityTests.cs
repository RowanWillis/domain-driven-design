﻿using Xunit;

namespace RowanWillis.Ddd.UnitTests;

public class EntityTests
{
	public static TheoryData<object> NonDefaultIds => new() {
		{ 1 },
		{ short.MaxValue },
		{ int.MaxValue },
		{ new Guid("948c688a-b704-4668-a737-4b11cc640143") }};

	public static TheoryData<object> DefaultIds => new() {
		{ default(int) },
		{ default(Guid) }};

	public static TheoryData<object, object> IdPairs => new() {
		{ 1, 2 },
		{ 512, 8497 },
		{
			new Guid("ee58b01c-05b7-4c1b-9c87-5f1583ecf4be"),
			new Guid("c3c76cd7-5fcc-4099-b47f-57d13ac09aea")
		}
	};

	[Theory]
	[MemberData(nameof(NonDefaultIds))]
	public void GivenANonDefaultId_AnEntityHasTheGivenId<TIdentity>(TIdentity id)
	{
		var entity = new SimpleEntityStub<TIdentity>(id);

		Assert.Equal(id, entity.Id);
	}

	[Theory]
	[MemberData(nameof(DefaultIds))]
	public void GivenADefaultId_AttemptingToCreateAnEntity_ThrowsAnException<TIdentity>(TIdentity id)
	{
		var exception = Assert.Throws<InvalidEntityIdException>(() => new SimpleEntityStub<TIdentity>(id));
		Assert.NotEmpty(exception.Message);
	}

	[Theory]
	[MemberData(nameof(NonDefaultIds))]
	public void EntitiesWithEqualIds_AreEqual<TIdentity>(TIdentity id)
	{
		var entity1 = new SimpleEntityStub<TIdentity>(id);
		var entity2 = new SimpleEntityStub<TIdentity>(id);

		Assert.Equal(entity1, entity2);
	}

	[Theory]
	[MemberData(nameof(NonDefaultIds))]
	public void EntitiesWithEqualIds_HaveTheSameHashCode<TIdentity>(TIdentity id)
	{
		var entity1 = new SimpleEntityStub<TIdentity>(id);
		var entity2 = new SimpleEntityStub<TIdentity>(id);

		Assert.Equal(entity1.GetHashCode(), entity2.GetHashCode());
	}

	[Theory]
	[MemberData(nameof(IdPairs))]
	public void EntitiesWithDistinctIds_AreNotEqual<TIdentity>(TIdentity id1, TIdentity id2)
	{
		var entity1 = new SimpleEntityStub<TIdentity>(id1);
		var entity2 = new SimpleEntityStub<TIdentity>(id2);

		Assert.NotEqual(entity1, entity2);
	}
}
