﻿using Xunit;

namespace RowanWillis.Ddd.UnitTests;

public class SequenceEqualityComponentTests
{
    public static readonly IEnumerable<object[]> Sequences = new object[][]
    {
        new[] { new[] { Array.Empty<object>() } },
        new[] { new[] { 1, 2, 3, 4, 5 } },
        new[] { new List<char> { 'a', 'b', 'c', 'd' } },
        new[] { new object[] { new object(), true, 100.00m, "test" } }
    };

    public static readonly IEnumerable<object[]> DistinctSequences = new object[][]
    {
        new[] { Array.Empty<object>(), new object[] { new object() } },
        new[] { new object[] {1, 2, 3, 4}, new object[] {0, 1, 2, 3} },
        new[] { new object[] { "a", "b", "c" }, new object[] { "A", "B", "C", "X", "Y", string.Empty } },
        new[] { new object[] { 'R', new object() }, new object?[] { null, "Test", 120.00m } }
    };

    [Theory]
    [MemberData(nameof(Sequences))]
    public void ANewSequenceEqualityComponent_HasTheGivenValues<TElement>(IEnumerable<TElement> sequence)
    {
        var component = new SequenceEqualityComponent<TElement>(sequence);

        Assert.Equal(sequence, component.Sequence);
    }

    [Fact]
    public void GivenNull_AttemptingToCreateaASequenceEqualityComponent_ThrowsArgumentNullException()
    {
#nullable disable
        _ = Assert.Throws<ArgumentNullException>(() => new SequenceEqualityComponent<object>(null));
#nullable enable
    }

    [Theory]
    [MemberData(nameof(Sequences))]
    public void TwoSequenceEqualityComponents_WithIdenticalSequences_AreEqual<TElement>(IEnumerable<TElement> sequence)
    {
        var component1 = new SequenceEqualityComponent<TElement>(sequence);
        var component2 = new SequenceEqualityComponent<TElement>(sequence);

        Assert.Equal(component1, component2);
    }

    [Theory]
    [MemberData(nameof(Sequences))]
    public void TwoSequenceEqualityComponents_WithIdenticalSequences_HaveTheSameHashCode<TElement>(IEnumerable<TElement> sequence)
    {
        var component1 = new SequenceEqualityComponent<TElement>(sequence);
        var component2 = new SequenceEqualityComponent<TElement>(sequence);

        Assert.Equal(component1.GetHashCode(), component2.GetHashCode());
    }

    [Theory]
    [MemberData(nameof(DistinctSequences))]
    public void TwoSequenceEqualityComponents_WithDifferentSequences_AreNotEqual(
        IEnumerable<object> sequence1,
        IEnumerable<object> sequence2)
    {
        var component1 = new SequenceEqualityComponent<object>(sequence1);
        var component2 = new SequenceEqualityComponent<object>(sequence2);

        Assert.NotEqual(component1, component2);
    }
}