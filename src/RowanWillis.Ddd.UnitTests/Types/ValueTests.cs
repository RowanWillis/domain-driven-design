﻿using Xunit;

namespace RowanWillis.Ddd.UnitTests;

public class ValueTests
{
	public static readonly TheoryData<object> Values = new()
	{
		1,
		"Test",
		new object(),
		true
	};

	public static TheoryData<object, object> ValueSequences = new()
	{
		{ 999, "Something" },
		{ new object(), false }
	};

	public static readonly TheoryData<object, object> DifferentValues = new()
	{
		{ 10, 2000 },
		{ "Test", "DifferentTest" },
		{ new object(), new object() },
		{ true, false }
	};

	public static readonly TheoryData<object, object> DifferentCompoundValues = new()
	{
		{
			new CompoundValueStub<decimal, bool>(3.14159m, false),
			new CompoundValueStub<decimal, bool>(3.14159m, true)
		},
		{
			new CompoundValueStub<object, object>(new object(), new object()),
			new CompoundValueStub<object, object>(new object(), new object())
		},
		{
			new CompoundValueStub<int, string>(500, "Test"),
			new CompoundValueStub<int, string>(600, "Test")
		}
	};

	[Theory]
	[MemberData(nameof(Values))]
	public void TwoSimpleValues_WithMatchingEqualityComponents_AreEqual<TValue>(TValue value)
	{
		var simpleValue1 = new SimpleValueStub<TValue>(value);
		var simpleValue2 = new SimpleValueStub<TValue>(value);

		Assert.Equal(simpleValue1, simpleValue2);
	}

	[Theory]
	[MemberData(nameof(Values))]
	public void TwoSimpleValues_WithMatchingEqualityComponents_HaveTheSameHashCode<TValue>(TValue value)
	{
		var simpleValue1 = new SimpleValueStub<TValue>(value);
		var simpleValue2 = new SimpleValueStub<TValue>(value);

		Assert.Equal(simpleValue1.GetHashCode(), simpleValue2.GetHashCode());
	}

	[Theory]
	[MemberData(nameof(DifferentValues))]
	public void TwoSimpleValues_WithDifferentEqualityComponents_AreNotEqual<TValue>(TValue value1, TValue value2)
	{
		var simpleValue1 = new SimpleValueStub<TValue>(value1);
		var simpleValue2 = new SimpleValueStub<TValue>(value2);

		Assert.NotEqual(simpleValue1, simpleValue2);
	}

	[Theory]
	[MemberData(nameof(ValueSequences))]
	public void TwoCompoundValues_WithMatchingEqualityComponents_AreEqual<TValue1, TValue2>(
		TValue1 component1, TValue2 component2)
	{
		var value1 = new CompoundValueStub<TValue1, TValue2>(component1, component2);
		var value2 = new CompoundValueStub<TValue1, TValue2>(component1, component2);

		Assert.Equal(value1, value2);
	}

	[Theory]
	[MemberData(nameof(ValueSequences))]
	public void TwoCompoundValues_WithMatchingEqualityComponents_HaveTheSameHashCode<TValue1, TValue2>(
		TValue1 component1, TValue2 component2)
	{
		var value1 = new CompoundValueStub<TValue1, TValue2>(component1, component2);
		var value2 = new CompoundValueStub<TValue1, TValue2>(component1, component2);

		Assert.Equal(value1, value2);
	}

	[Theory]
	[MemberData(nameof(DifferentCompoundValues))]
	public void TwoCompoundValues_WithDifferentEqualityComponents_AreNotEqual<TValue1, TValue2>(
		CompoundValueStub<TValue1, TValue2> value1,
		CompoundValueStub<TValue1, TValue2> value2)
	{
		Assert.NotEqual(value1, value2);
	}
}