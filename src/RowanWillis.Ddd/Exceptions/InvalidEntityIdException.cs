﻿namespace RowanWillis.Ddd;

public class InvalidEntityIdException : Exception
{
    public override string Message => "Entity ID must be a non-default value.";
}
