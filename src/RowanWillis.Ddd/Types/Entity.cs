﻿namespace RowanWillis.Ddd;

public abstract class Entity<TIdentity>
{
    public Entity(TIdentity id)
    {
        if (id?.Equals(default(TIdentity)) ?? true)
            throw new InvalidEntityIdException();

        Id = id;
    }

    public TIdentity Id { get; }

    public override bool Equals(object? obj)
    {
        if (obj is not Entity<TIdentity> other)
            return false;

        if (ReferenceEquals(this, other))
            return true;

        return Id!.Equals(other.Id);
    }

    public static bool operator ==(Entity<TIdentity>? a, Entity<TIdentity>? b)
    {
        if (a is null && b is null)
            return true;

        if (a is null || b is null)
            return false;

        return a.Equals(b);
    }

    public static bool operator !=(Entity<TIdentity>? a, Entity<TIdentity>? b)
        => !(a == b);

    public override int GetHashCode()
        => HashCode.Combine(Id, GetType());
}