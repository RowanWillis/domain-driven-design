﻿namespace RowanWillis.Ddd;

public sealed class SequenceEqualityComponent<TElement>
{
    public SequenceEqualityComponent(IEnumerable<TElement> sequence)
    {
        Sequence = sequence ?? throw new ArgumentNullException(nameof(sequence));
    }

    public IEnumerable<TElement> Sequence { get; }

    public override bool Equals(object? obj)
        => obj is SequenceEqualityComponent<TElement> other && EqualsCore(other);

    private bool EqualsCore(SequenceEqualityComponent<TElement> other)
        => Sequence.SequenceEqual(other.Sequence);

    public override int GetHashCode()
        => Sequence.Aggregate(1, (current, obj) => HashCode.Combine(current, obj));

    public static bool operator ==(
        SequenceEqualityComponent<TElement>? a,
        SequenceEqualityComponent<TElement>? b)
    {
        if (a is null && b is null)
            return true;

        if (a is null || b is null)
            return false;

        return a.Equals(b);
    }

    public static bool operator !=(
        SequenceEqualityComponent<TElement>? a,
        SequenceEqualityComponent<TElement>? b)
        => !(a == b);
}