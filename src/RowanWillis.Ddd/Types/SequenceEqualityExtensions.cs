﻿namespace RowanWillis.Ddd;

public static class SequenceEqualityExtensions
{
    public static SequenceEqualityComponent<TElement> AsEqualityComponent<TElement>(
        this IEnumerable<TElement> sequence)
        => new SequenceEqualityComponent<TElement>(sequence);
}