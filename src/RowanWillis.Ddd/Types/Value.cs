﻿namespace RowanWillis.Ddd;
    
public abstract class Value
{
    protected abstract IEnumerable<object?> EqualityComponents { get; }

    public override bool Equals(object? obj)
        => obj is not null
            && GetType() == obj.GetType()
            && EqualityComponents.SequenceEqual(((Value)obj).EqualityComponents);

    public override int GetHashCode()
        => EqualityComponents
            .Aggregate(1, (current, obj) => HashCode.Combine(current, obj));

    public static bool operator ==(Value? a, Value? b)
    {
        if (a is null && b is null)
            return true;

        if (a is null || b is null)
            return false;

        return a.Equals(b);
    }

    public static bool operator !=(Value? a, Value? b)
        => !(a == b);
}